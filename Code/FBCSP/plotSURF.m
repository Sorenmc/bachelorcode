% DESCRIPTION OF Script
% Used to 3d plot the multivariate variated data, and save them as a new
% file.
%
% AUTHORS OF FUNCTION:
% Soeren Moeller Christensen, s153571@student.dtu.dk
% Nicklas Stubkjaer Holm, s154411@student.dtu.dk



figure()
LDAData = load('OVRLDAData.mat');
LDAData = LDAData.LDAData;
LDAData = squeeze(mean(LDAData));
surf(LDAData)
print('FBCSP/plots/OVRLDAplot','-depsc')


figure()
SVMData = load('OVRSVMData.mat');
SVMData = SVMData.SVMData;
SVMData = squeeze(mean(SVMData));
surf(SVMData)
print('FBCSP/plots/OVRSVMplot','-depsc')



figure()
NABData = load('OVRNABData.mat');
NABData = NABData.NABData;
NABData = squeeze(mean(NABData));
surf(NABData)
print('FBCSP/plots/OVRSVMplot')
